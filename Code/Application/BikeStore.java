package Application;
import Vehicles.Bicycle;

public class BikeStore
{
    public static void main(String[] args)
    {
        Bicycle[] arrBicycle = new Bicycle[4];
        arrBicycle[0]= new Bicycle("mazda",2,60);
        arrBicycle[1]= new Bicycle("mercedes",10,50);
        arrBicycle[2]= new Bicycle("bmw",2,40);
        arrBicycle[3]= new Bicycle("honda",7,30);
    

    for (Bicycle i : arrBicycle) 
       {
            System.out.println(i);
       }
    }

}